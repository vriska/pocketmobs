package dev.vriska.pocketmobs.item;

import com.mojang.serialization.MapCodec;
import dev.vriska.pocketmobs.PocketMobsSoundEvents;
import dev.vriska.pocketmobs.entity.ThrownCapturedMobBallEntity;
import net.minecraft.client.item.TooltipConfig;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.NbtComponent;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import java.util.List;

public class CapturedMobBallItem extends Item {
    private static final MapCodec<EntityType<?>> CODEC = Registries.ENTITY_TYPE.getCodec().fieldOf("id");

    public CapturedMobBallItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);

        world.playSound(null, user.getX(), user.getY(), user.getZ(), PocketMobsSoundEvents.ENTITY_MOB_BALL_THROW,
                        SoundCategory.NEUTRAL, 0.5F, 0.4F / (world.getRandom().nextFloat() * 0.4F + 0.8F));

        if (user instanceof ServerPlayerEntity player) {
            ThrownCapturedMobBallEntity thrownCapturedMobBallEntity = new ThrownCapturedMobBallEntity(user, world, player);
            thrownCapturedMobBallEntity.setItem(itemStack);
            thrownCapturedMobBallEntity.setProperties(user, user.getPitch(), user.getYaw(), 0.0F, 1.5F, 1.0F);
            world.spawnEntity(thrownCapturedMobBallEntity);
        }

        itemStack.decrement(1);
        return TypedActionResult.success(itemStack, world.isClient());
    }

    @Override
    public void appendTooltip(ItemStack stack, TooltipContext context, List<Text> tooltip, TooltipConfig config) {
        NbtComponent component = stack.get(DataComponentTypes.ENTITY_DATA);
        if (component == null) return;
        EntityType<?> type = component.read(CODEC).result().orElse(null);
        if (type == null) return;
        tooltip.add(type.getName().copy().formatted(Formatting.GRAY));
    }
}
