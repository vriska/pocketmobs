package dev.vriska.pocketmobs.item;

import dev.vriska.pocketmobs.PocketMobsSoundEvents;
import dev.vriska.pocketmobs.entity.ThrownMobBallEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class MobBallItem extends Item {
    public MobBallItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);

        world.playSound(null, user.getX(), user.getY(), user.getZ(), PocketMobsSoundEvents.ENTITY_MOB_BALL_THROW,
                        SoundCategory.NEUTRAL, 0.5F, 0.4F / (world.getRandom().nextFloat() * 0.4F + 0.8F));

        if (!world.isClient()) {
            ThrownMobBallEntity thrownMobBallEntity = new ThrownMobBallEntity(user, world);
            thrownMobBallEntity.setItem(itemStack);
            thrownMobBallEntity.setProperties(user, user.getPitch(), user.getYaw(), 0.0F, 1.5F, 1.0F);
            world.spawnEntity(thrownMobBallEntity);
        }

        itemStack.decrement(1);
        return TypedActionResult.success(itemStack, world.isClient());
    }
}
