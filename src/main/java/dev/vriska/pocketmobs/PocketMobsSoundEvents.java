package dev.vriska.pocketmobs;

import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;

public class PocketMobsSoundEvents {
    public static final SoundEvent ENTITY_MOB_BALL_THROW = SoundEvent.createVariableRangeEvent(
        Identifier.of("pocketmobs", "entity.mob_ball.throw"));

    public static void init() {
        PocketMobs.AUTOREG.autoRegister(x -> Registry.register(Registries.SOUND_EVENT, x.getId(), x),
                                        PocketMobsSoundEvents.class, SoundEvent.class);
    }
}
