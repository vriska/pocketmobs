package dev.vriska.pocketmobs.entity;

import dev.vriska.pocketmobs.PocketMobs;
import dev.vriska.pocketmobs.PocketMobsEntities;
import dev.vriska.pocketmobs.PocketMobsItems;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.NbtComponent;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.projectile.thrown.ThrownItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.world.World;

public class ThrownMobBallEntity extends ThrownItemEntity {
    public ThrownMobBallEntity(EntityType<ThrownMobBallEntity> entityType, World world) {
        super(entityType, world);
    }

    public ThrownMobBallEntity(LivingEntity livingEntity, World world) {
        super(PocketMobsEntities.THROWN_MOB_BALL, livingEntity, world);
    }

    @Override
    protected Item getDefaultItem() {
        return PocketMobsItems.MOB_BALL;
    }

    @Override
    protected void onBlockHit(BlockHitResult blockHitResult) {
        super.onBlockHit(blockHitResult);
        if (!this.getWorld().isClient) {
            this.discard();
            this.getWorld()
                .spawnEntity(new ItemEntity(this.getWorld(), this.getX(), this.getY(), this.getZ(), this.getStack()));
        }
    }

    @Override
    protected void onEntityHit(EntityHitResult entityHitResult) {
        super.onEntityHit(entityHitResult);

        if (!this.getWorld().isClient() && entityHitResult.getEntity() instanceof MobEntity entity) {
            if (entity.isInvulnerable()) return;

            if (PocketMobs.POKEMON_SET_MANAGER.get(EntityType.getId(entity.getType())).isEmpty()) return;

            NbtCompound nbt = new NbtCompound();
            if (!entity.saveSelfNbt(nbt)) return;

            ItemStack itemStack = new ItemStack(PocketMobsItems.CAPTURED_MOB_BALL);
            itemStack.set(DataComponentTypes.ENTITY_DATA, NbtComponent.of(nbt));

            entity.remove(RemovalReason.KILLED);

            this.discard();
            this.getWorld()
                .spawnEntity(new ItemEntity(this.getWorld(), this.getX(), this.getY(), this.getZ(), itemStack));
        }
    }
}
