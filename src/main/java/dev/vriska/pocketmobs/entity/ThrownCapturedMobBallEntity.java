package dev.vriska.pocketmobs.entity;

import dev.vriska.pocketmobs.PocketMobsEntities;
import dev.vriska.pocketmobs.PocketMobsItems;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.NbtComponent;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.thrown.ThrownItemEntity;
import net.minecraft.item.Item;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

import java.util.List;

public class ThrownCapturedMobBallEntity extends ThrownItemEntity {
    private ServerPlayerEntity player = null;

    public ThrownCapturedMobBallEntity(EntityType<ThrownCapturedMobBallEntity> entityType, World world) {
        super(entityType, world);
    }

    public ThrownCapturedMobBallEntity(LivingEntity livingEntity, World world, ServerPlayerEntity player) {
        super(PocketMobsEntities.THROWN_CAPTURED_MOB_BALL, livingEntity, world);
        this.player = player;
    }

    @Override
    protected Item getDefaultItem() {
        return PocketMobsItems.CAPTURED_MOB_BALL;
    }

    @Override
    protected void onBlockHit(BlockHitResult blockHitResult) {
        super.onBlockHit(blockHitResult);
        if (!this.getWorld().isClient) {
            this.discard();

            NbtComponent component = this.getStack().get(DataComponentTypes.ENTITY_DATA);
            if (component == null) {
                this.getWorld()
                    .spawnEntity(
                        new ItemEntity(this.getWorld(), this.getX(), this.getY(), this.getZ(), this.getStack()));
                return;
            }

            List<CapturedMobEntity> nearbyEntities = this.getWorld()
                .getEntitiesByType(PocketMobsEntities.CAPTURED_MOB, new Box(this.getBlockPos()).expand(5.0, 0.0, 5.0),
                                   capturedMobEntity -> !capturedMobEntity.isInBattle());

            CapturedMobEntity entity = new CapturedMobEntity(this.getWorld(), component.copy(), player);
            //noinspection SuspiciousNameCombination
            float yaw = (float) (-MathHelper.atan2(getVelocity().x, getVelocity().z) * 180 / Math.PI);
            entity.moveTo(this.getPos(), yaw, 0);
            this.getWorld().spawnEntity(entity);

            if (!nearbyEntities.isEmpty()) {
                entity.tryStartBattle(nearbyEntities.getFirst());
            }
        }
    }
}
