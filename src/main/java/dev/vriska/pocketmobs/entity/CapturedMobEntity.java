package dev.vriska.pocketmobs.entity;

import dev.vriska.pocketmobs.PocketMobs;
import dev.vriska.pocketmobs.PocketMobsEntities;
import dev.vriska.pocketmobs.PocketMobsItems;
import dev.vriska.pocketmobs.battle.Battle;
import dev.vriska.pocketmobs.battle.BattleSide;
import dev.vriska.pocketmobs.battle.HasCurrentBattle;
import dev.vriska.pocketmobs.set.PokemonSet;
import net.minecraft.command.argument.EntityAnchorArgumentType;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.NbtComponent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.MovementType;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class CapturedMobEntity extends Entity {
    private static final TrackedData<NbtCompound> ENTITY = DataTracker.registerData(CapturedMobEntity.class,
                                                                                    TrackedDataHandlerRegistry.TAG_COMPOUND);
    private static final TrackedData<Integer> HURT_TIME = DataTracker.registerData(CapturedMobEntity.class,
                                                                                   TrackedDataHandlerRegistry.INTEGER);

    private MobEntity entity = null;
    private int returnTimer = 100;
    public Battle battle = null;
    private BattleSide side = null;
    private ServerPlayerEntity player = null;

    public CapturedMobEntity(EntityType<CapturedMobEntity> entityType, World world) {
        super(entityType, world);
    }

    public CapturedMobEntity(World world, NbtCompound entityNbt, ServerPlayerEntity player) {
        super(PocketMobsEntities.CAPTURED_MOB, world);
        this.dataTracker.set(ENTITY, entityNbt);
        this.player = player;
    }

    @Override
    protected void initDataTracker(DataTracker.Builder builder) {
        builder.add(ENTITY, new NbtCompound());
        builder.add(HURT_TIME, 0);
    }

    @Override
    protected void readCustomDataFromNbt(NbtCompound nbt) {
        this.dataTracker.set(ENTITY, nbt.getCompound("entity"));
    }

    @Override
    protected void writeCustomDataToNbt(NbtCompound nbt) {
        nbt.put("entity", this.dataTracker.get(ENTITY));
    }

    public MobEntity getEntity() {
        if (entity == null) {
            NbtCompound entityNbt = this.dataTracker.get(ENTITY);
            Entity read = EntityType.getEntityFromNbt(entityNbt, this.getWorld()).orElse(null);
            if (read instanceof MobEntity x) {
                entity = x;
            } else {
                return null;
            }
        }
        entity.moveTo(this.getPos(), this.getYaw(), this.getPitch());
        entity.bodyYaw = entity.prevBodyYaw = entity.headYaw = entity.prevHeadYaw = this.getYaw();
        entity.setOnFire(false);
        entity.hurtTime = entity.maxHurtTime = this.dataTracker.get(HURT_TIME);
        return entity;
    }

    public void setEntity(MobEntity entity) {
        this.entity = entity;

        NbtCompound entityNbt = new NbtCompound();
        entity.saveSelfNbt(entityNbt);
        this.dataTracker.set(ENTITY, entityNbt);
    }

    @Override
    protected double getDefaultGravity() {
        return this.getEntity().getGravity();
    }

    @Override
    public void tick() {
        super.tick();
        this.applyGravity();
        // Is this an ItemEntity?
        if (!this.isOnGround() || this.getVelocity().horizontalLengthSquared() > 1.0E-5F ||
            (this.age + this.getId()) % 4 == 0) {
            this.move(MovementType.SELF, this.getVelocity());
            float f = 0.98F;
            if (this.isOnGround()) {
                f = this.getWorld().getBlockState(this.getVelocityAffectingPos()).getBlock().getSlipperiness() * 0.98F;
            }

            this.setVelocity(this.getVelocity().multiply(f, 0.98, f));
            if (this.isOnGround()) {
                Vec3d vec3d2 = this.getVelocity();
                if (vec3d2.y < 0.0) {
                    this.setVelocity(vec3d2.multiply(1.0, -0.5, 1.0));
                }
            }
        }

        if (!this.getWorld().isClient()) {
            int hurtTime = this.dataTracker.get(HURT_TIME);
            if (hurtTime > 0) {
                hurtTime--;
                this.dataTracker.set(HURT_TIME, hurtTime);
            }
        }

        if (!this.getWorld().isClient() && !isInBattle() && --this.returnTimer <= 0) {
            ItemStack itemStack = new ItemStack(PocketMobsItems.CAPTURED_MOB_BALL);
            NbtCompound nbt = this.dataTracker.get(ENTITY);
            itemStack.set(DataComponentTypes.ENTITY_DATA, NbtComponent.of(nbt));
            this.discard();
            this.getWorld()
                .spawnEntity(new ItemEntity(this.getWorld(), this.getX(), this.getY(), this.getZ(), itemStack));
        }

        if (isInBattle()) battle.handleMessages();
        if (isInBattle()) battle.showRequest(side);
    }

    public boolean isInBattle() {
        return battle != null;
    }

    public void tryStartBattle(CapturedMobEntity opponent) {
        if (player == null) return;
        Battle battle = new Battle(this, this.player, opponent, opponent.player);
        this.battle = battle;
        this.side = BattleSide.P1;
        opponent.battle = battle;
        opponent.side = BattleSide.P2;
        ((HasCurrentBattle) this.player).setCurrentBattle(battle, BattleSide.P1);
        ((HasCurrentBattle) opponent.player).setCurrentBattle(battle, BattleSide.P2);
        this.lookAt(EntityAnchorArgumentType.EntityAnchor.EYES, opponent.getPos());
        opponent.lookAt(EntityAnchorArgumentType.EntityAnchor.EYES, this.getPos());
    }

    public String toPacked() {
        Identifier id = EntityType.getId(getEntity().getType());
        PokemonSet set = PocketMobs.POKEMON_SET_MANAGER.get(id).orElse(null);

        if (set == null) {
            String nickname = getEntity().getType().getUntranslatedName();
            String species = "Bulbasaur";
            String ability = "0";
            String moves = "Tackle,Growl";
            return String.format("%s|%s||%s|%s|||||||", nickname, species, ability, moves);
        }

        return set.asPacked();
    }

    public void hurt() {
        MobEntity entity = getEntity();
        entity.handleDamagingEvent(entity.getDamageSources().generic());
        this.dataTracker.set(HURT_TIME, entity.maxHurtTime == 0 ? 20 : entity.maxHurtTime);
    }
}
