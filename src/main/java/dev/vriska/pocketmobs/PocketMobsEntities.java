package dev.vriska.pocketmobs;

import dev.vriska.pocketmobs.entity.CapturedMobEntity;
import dev.vriska.pocketmobs.entity.ThrownCapturedMobBallEntity;
import dev.vriska.pocketmobs.entity.ThrownMobBallEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.registry.Registries;

@SuppressWarnings("unused")
public class PocketMobsEntities {
    public static final EntityType<ThrownMobBallEntity> THROWN_MOB_BALL =
        EntityType.Builder.<ThrownMobBallEntity>create(
                ThrownMobBallEntity::new, SpawnGroup.MISC)
            .setDimensions(0.25F, 0.25F)
            .maxTrackingRange(4)
            .trackingTickInterval(10)
            .build();

    public static final EntityType<ThrownCapturedMobBallEntity> THROWN_CAPTURED_MOB_BALL =
        EntityType.Builder.<ThrownCapturedMobBallEntity>create(
                ThrownCapturedMobBallEntity::new, SpawnGroup.MISC)
            .setDimensions(0.25F, 0.25F)
            .maxTrackingRange(4)
            .trackingTickInterval(10)
            .build();

    public static final EntityType<CapturedMobEntity> CAPTURED_MOB = EntityType.Builder.<CapturedMobEntity>create(
            CapturedMobEntity::new, SpawnGroup.MISC)
        .setDimensions(0.25F, 0.25F)
        .maxTrackingRange(4)
        .trackingTickInterval(10)
        .build();

    public static void init() {
        PocketMobs.AUTOREG.autoRegister(Registries.ENTITY_TYPE, PocketMobsEntities.class, EntityType.class);
    }
}
