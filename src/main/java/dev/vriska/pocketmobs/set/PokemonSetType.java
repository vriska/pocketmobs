package dev.vriska.pocketmobs.set;

import com.mojang.serialization.Lifecycle;
import com.mojang.serialization.MapCodec;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.SimpleRegistry;
import net.minecraft.util.Identifier;

public record PokemonSetType<T extends PokemonSet>(MapCodec<T> codec) {
    public static final Registry<PokemonSetType<?>> REGISTRY = new SimpleRegistry<>(
        RegistryKey.ofRegistry(Identifier.of("pocketmobs", "pokemon_set_types")), Lifecycle.stable());
}
