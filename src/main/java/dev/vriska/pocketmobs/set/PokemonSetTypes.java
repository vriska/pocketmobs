package dev.vriska.pocketmobs.set;

import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

@SuppressWarnings("unused")
public class PokemonSetTypes {
    public static final PokemonSetType<PackedSet> PACKED_SET = Registry.register(PokemonSetType.REGISTRY,
                                                                                 Identifier.of("pocketmobs",
                                                                                               "packed_set"),
                                                                                 new PokemonSetType<>(PackedSet.CODEC));

    public static void init() {
    }
}
