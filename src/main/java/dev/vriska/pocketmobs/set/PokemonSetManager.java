package dev.vriska.pocketmobs.set;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.mojang.serialization.JsonOps;
import dev.vriska.pocketmobs.PocketMobs;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.registry.ResourceFileNamespace;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;

import java.io.Reader;
import java.util.Map;
import java.util.Optional;

public class PokemonSetManager implements SimpleSynchronousResourceReloadListener {
    private Map<Identifier, PokemonSet> sets;

    @Override
    public Identifier getFabricId() {
        return Identifier.of("pocketmobs", "pokemon_sets");
    }

    @Override
    public void reload(ResourceManager manager) {
        ResourceFileNamespace ns = ResourceFileNamespace.json("pokemon_sets");

        ImmutableMap.Builder<Identifier, PokemonSet> builder = ImmutableMap.builder();

        for (Map.Entry<Identifier, Resource> entry : ns.findMatchingResources(manager).entrySet()) {
            Identifier path = entry.getKey();
            Identifier id = ns.unwrapFilePath(path);

            try (Reader reader = entry.getValue().openBufferedReader()) {
                JsonElement json = JsonParser.parseReader(reader);
                PokemonSet set = PokemonSet.CODEC.parse(JsonOps.INSTANCE, json).getOrThrow(JsonParseException::new);
                builder.put(id, set);
            } catch (Exception e) {
                PocketMobs.LOGGER.error("Couldn't parse data file {} from {}", id, path, e);
            }
        }

        this.sets = builder.build();
        PocketMobs.LOGGER.info("Loaded {} sets", this.sets.size());
    }

    public Optional<PokemonSet> get(Identifier id) {
        return Optional.ofNullable(this.sets.get(id));
    }
}
