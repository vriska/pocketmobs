package dev.vriska.pocketmobs.set;

import com.mojang.serialization.Codec;

public interface PokemonSet {
    Codec<PokemonSet> CODEC = PokemonSetType.REGISTRY.getCodec().dispatch(PokemonSet::getType, PokemonSetType::codec);

    PokemonSetType<?> getType();

    String asPacked();
}
