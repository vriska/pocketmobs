package dev.vriska.pocketmobs.set;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

public class PackedSet implements PokemonSet {
    public static final MapCodec<PackedSet> CODEC = RecordCodecBuilder.mapCodec(
        instance -> instance.group(Codec.STRING.fieldOf("set").forGetter(PackedSet::asPacked))
            .apply(instance, PackedSet::new));

    private final String set;

    public PackedSet(String set) {
        this.set = set;
    }

    @Override
    public String asPacked() {
        return set;
    }

    @Override
    public PokemonSetType<?> getType() {
        return null;
    }
}
