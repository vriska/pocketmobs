package dev.vriska.pocketmobs.client;

import dev.vriska.pocketmobs.PocketMobsEntities;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.FlyingItemEntityRenderer;

@SuppressWarnings("deprecation")
public class PocketMobsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(PocketMobsEntities.THROWN_MOB_BALL, FlyingItemEntityRenderer::new);
        EntityRendererRegistry.register(PocketMobsEntities.THROWN_CAPTURED_MOB_BALL, FlyingItemEntityRenderer::new);
        EntityRendererRegistry.register(PocketMobsEntities.CAPTURED_MOB, CapturedMobEntityRenderer::new);
    }
}
