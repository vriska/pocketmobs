package dev.vriska.pocketmobs.client;

import dev.vriska.pocketmobs.entity.CapturedMobEntity;
import net.minecraft.client.render.Frustum;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3d;

@SuppressWarnings("unchecked")
public class CapturedMobEntityRenderer extends EntityRenderer<CapturedMobEntity> {
    public CapturedMobEntityRenderer(EntityRendererFactory.Context ctx) {
        super(ctx);
    }

    @SuppressWarnings("rawtypes")
    private EntityRenderer getRenderer(CapturedMobEntity entity) {
        return this.dispatcher.getRenderer(entity.getEntity());
    }

    @Override
    public Identifier getTexture(CapturedMobEntity entity) {
        return getRenderer(entity).getTexture(entity.getEntity());
    }

    @Override
    public boolean shouldRender(CapturedMobEntity entity, Frustum frustum, double x, double y, double z) {
        return getRenderer(entity).shouldRender(entity.getEntity(), frustum, x, y, z);
    }

    @Override
    public Vec3d getPositionOffset(CapturedMobEntity entity, float tickDelta) {
        return getRenderer(entity).getPositionOffset(entity.getEntity(), tickDelta);
    }

    @Override
    public void render(CapturedMobEntity entity, float yaw, float tickDelta, MatrixStack matrices,
        VertexConsumerProvider vertexConsumers, int light) {
        getRenderer(entity).render(entity.getEntity(), yaw, 0.0f, matrices, vertexConsumers, light);
    }
}
