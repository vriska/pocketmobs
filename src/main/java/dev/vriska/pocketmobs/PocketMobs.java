package dev.vriska.pocketmobs;

import com.unascribed.lib39.core.api.AutoRegistry;
import com.unascribed.lib39.dessicant.api.DessicantControl;
import dev.vriska.pocketmobs.set.PokemonSetManager;
import dev.vriska.pocketmobs.set.PokemonSetTypes;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.resource.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("deprecation")
public class PocketMobs implements ModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("pocketmobs");
    public static final AutoRegistry AUTOREG = AutoRegistry.of("pocketmobs");

    public static final PokemonSetManager POKEMON_SET_MANAGER = new PokemonSetManager();

    @Override
    public void onInitialize() {
        DessicantControl.optIn("pocketmobs");
        PocketMobsItems.init();
        PocketMobsEntities.init();
        PocketMobsSoundEvents.init();
        PokemonSetTypes.init();

        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(POKEMON_SET_MANAGER);
    }
}
