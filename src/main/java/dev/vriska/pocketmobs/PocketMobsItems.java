package dev.vriska.pocketmobs;

import dev.vriska.pocketmobs.item.CapturedMobBallItem;
import dev.vriska.pocketmobs.item.MobBallItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;

@SuppressWarnings("unused")
public class PocketMobsItems {
    public static final MobBallItem MOB_BALL = new MobBallItem(new Item.Settings());
    public static final CapturedMobBallItem CAPTURED_MOB_BALL = new CapturedMobBallItem(new Item.Settings());

    public static void init() {
        PocketMobs.AUTOREG.autoRegister(Registries.ITEM, PocketMobsItems.class, Item.class);
    }
}
