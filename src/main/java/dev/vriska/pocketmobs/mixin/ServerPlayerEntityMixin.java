package dev.vriska.pocketmobs.mixin;

import dev.vriska.pocketmobs.battle.Battle;
import dev.vriska.pocketmobs.battle.BattleSide;
import dev.vriska.pocketmobs.battle.HasCurrentBattle;
import net.minecraft.server.network.ServerPlayerEntity;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@SuppressWarnings("AddedMixinMembersNamePattern")
@Mixin(ServerPlayerEntity.class)
public class ServerPlayerEntityMixin implements HasCurrentBattle {
    @Unique
    Battle battle;

    @Unique
    BattleSide side;

    @Override
    public @Nullable Battle getCurrentBattle() {
        return battle;
    }

    @Override
    public @Nullable BattleSide getBattleSide() {
        return side;
    }

    @Override
    public void setCurrentBattle(@Nullable Battle battle, @Nullable BattleSide side) {
        this.battle = battle;
        this.side = side;
    }
}
