package dev.vriska.pocketmobs.mixin;

import dev.vriska.pocketmobs.battle.Battle;
import dev.vriska.pocketmobs.battle.HasCurrentBattle;
import net.minecraft.network.message.FilteredText;
import net.minecraft.network.message.SignedChatMessage;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayNetworkHandler.class)
public class ServerPlayNetworkHandlerMixin {
    @Shadow
    public ServerPlayerEntity player;

    @Inject(method = "method_45064", at = @At("HEAD"), cancellable = true)
    private void onChatMessage(SignedChatMessage signedChatMessage, Text text, FilteredText filteredText,
        CallbackInfo ci) {
        if (!filteredText.raw().startsWith(">")) return;
        Battle battle = ((HasCurrentBattle) player).getCurrentBattle();
        if (battle == null) return;
        ci.cancel();
        if (battle.write(player, filteredText.raw())) {
            player.sendSystemMessage(Text.literal(filteredText.raw()).formatted(Formatting.DARK_GRAY));
        } else {
            player.sendSystemMessage(Text.translatable("pocketmobs.battle.wrong_player"));
        }
    }
}
