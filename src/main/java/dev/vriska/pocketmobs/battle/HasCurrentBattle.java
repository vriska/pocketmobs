package dev.vriska.pocketmobs.battle;

import org.jetbrains.annotations.Nullable;

public interface HasCurrentBattle {
    @Nullable
    Battle getCurrentBattle();

    @Nullable
    BattleSide getBattleSide();

    void setCurrentBattle(@Nullable Battle battle, @Nullable BattleSide side);
}
