package dev.vriska.pocketmobs.battle;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.io.FileSystem;
import org.graalvm.polyglot.io.IOAccess;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Consumer;

public class Showdown implements AutoCloseable {
    private final Context context;
    private final Value battleStream;

    public Showdown() {
        @SuppressWarnings("deprecation") ModContainer mod = FabricLoader.getInstance()
            .getModContainer("pocketmobs")
            .orElseThrow();
        Path zip = mod.findPath("showdown.zip").orElseThrow();
        FileSystem fs;
        try {
            fs = FileSystem.newFileSystem(FileSystems.newFileSystem(zip));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        IOAccess io = IOAccess.newBuilder().fileSystem(fs).build();
        context = Context.newBuilder("js")
            .allowAllAccess(true)
            .allowIO(io)
            .option("js.commonjs-require", "true")
            .option("js.commonjs-require-cwd", "/")
            .build();
        Value module = context.eval("js", "require('./sim/battle-stream')");
        battleStream = module.getMember("BattleStream");
    }

    public BattleStream createBattle() {
        Value inner = battleStream.newInstance();
        return new BattleStream(inner);
    }

    @Override
    public void close() {
        context.close();
    }

    public static class BattleStream {
        private final Value stream;
        private final Queue<String> queue = new ConcurrentLinkedDeque<>();
        private boolean waiting = false;

        private BattleStream(Value stream) {
            this.stream = stream;
        }

        private void acceptMessage(Object message) {
            queue.add((String) message);
            Consumer<Object> callback = this::acceptMessage;
            stream.invokeMember("read").invokeMember("then", callback);
        }

        public Optional<String> read() {
            return Optional.ofNullable(queue.poll());
        }

        public void write(String message) {
            stream.invokeMember("write", message);
            if (!waiting) {
                waiting = true;
                Consumer<Object> callback = this::acceptMessage;
                stream.invokeMember("read").invokeMember("then", callback);
            }
        }
    }
}
