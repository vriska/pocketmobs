package dev.vriska.pocketmobs.battle;

import com.google.gson.Gson;
import dev.vriska.pocketmobs.PocketMobs;
import dev.vriska.pocketmobs.entity.CapturedMobEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.component.StringComponent;
import net.minecraft.util.Formatting;

import java.lang.ref.Cleaner;
import java.lang.ref.WeakReference;
import java.util.Optional;
import java.util.concurrent.*;

public class Battle implements AutoCloseable {
    public CapturedMobEntity p1Mob;
    public ServerPlayerEntity player1;
    public CapturedMobEntity p2Mob;
    public ServerPlayerEntity player2;

    private static final Cleaner cleaner = Cleaner.create();

    private final Cleaner.Cleanable cleanable;
    private final Gson gson;

    private final BlockingQueue<String> writeQueue = new LinkedBlockingQueue<>();
    private Showdown.BattleStream stream = null;

    private static final ExecutorService showdownPool = Executors.newCachedThreadPool();
    private static final ThreadLocal<Showdown> showdown = ThreadLocal.withInitial(Showdown::new);
    private String p1Request;
    private String p2Request;

    public Battle(CapturedMobEntity p1Mob, ServerPlayerEntity player1, CapturedMobEntity p2Mob,
        ServerPlayerEntity player2) {
        this.p1Mob = p1Mob;
        this.player1 = player1;
        this.p2Mob = p2Mob;
        this.player2 = player2;

        WeakReference<Battle> ref = new WeakReference<>(this);
        Future<?> battleFuture = showdownPool.submit(new BattleFuture(ref, writeQueue));

        State state = new State(battleFuture);
        cleanable = cleaner.register(this, state);

        writeQueue.add(">start {\"formatid\":\"gen9customgame, ! Team Preview\"}");

        gson = new Gson();
        Player p1 = new Player(player1.getProfileName(), p1Mob.toPacked());
        Player p2 = new Player(player2.getProfileName(), p2Mob.toPacked());
        writeQueue.add(">player p1 " + gson.toJson(p1));
        writeQueue.add(">player p2 " + gson.toJson(p2));
    }

    @Override
    public void close() {
        cleanable.clean();
        p1Mob.battle = null;
        p2Mob.battle = null;
        ((HasCurrentBattle) player1).setCurrentBattle(null, null);
        ((HasCurrentBattle) player2).setCurrentBattle(null, null);
    }

    public Optional<String> read() {
        return Optional.ofNullable(stream).flatMap(Showdown.BattleStream::read);
    }

    public boolean write(ServerPlayerEntity player, String message) {
        if (message.startsWith(">p1 ")) {
            if (player != player1) return false;
        } else if (message.startsWith(">p2 ")) {
            if (player != player2) return false;
        } else {
            message = (player == player1 ? ">p1 " : ">p2 ") + message.substring(1);
        }
        writeQueue.add(message);
        return true;
    }

    public void handleMessages() {
        Optional<String> message;
        while ((message = read()).isPresent()) {
            processMessage(message.get());
        }
    }

    private void processMessage(String message) {
        String[] lines = message.split("\n");
        switch (lines[0]) {
            case "update" -> {
                for (int i = 1; i < lines.length; i++) {
                    if (lines[i].isEmpty()) continue;
                    if (lines[i].startsWith("|split|")) {
                        processUpdate(player1, BattleSide.P1, lines[i + 1]);
                        processUpdate(player2, BattleSide.P2, lines[i + 2]);
                        i += 2;
                        continue;
                    }
                    processUpdate(player1, BattleSide.P1, lines[i]);
                    processUpdate(player2, BattleSide.P2, lines[i]);
                }
            }
            case "sideupdate" -> {
                ServerPlayerEntity player = lines[1].equals("p1") ? player1 : player2;
                BattleSide side = lines[1].equals("p1") ? BattleSide.P1 : BattleSide.P2;
                for (int i = 2; i < lines.length; i++) {
                    if (!lines[i].isEmpty()) processUpdate(player, side, lines[i]);
                }
            }
            case "end" -> close();
            default -> PocketMobs.LOGGER.warn("unknown message type: {}", lines[0]);
        }
    }

    private void processUpdate(ServerPlayerEntity player, BattleSide side, String update) {
        String[] split = update.split("\\|");
        if (split.length < 2) return;
        switch (split[1]) {
            case "start" -> player.sendSystemMessage(Text.translatable("pocketmobs.battle.start"));
            case "turn" -> player.sendSystemMessage(
                Text.translatable("pocketmobs.battle.turn", split[2]).formatted(Formatting.BOLD));
            case "win" -> player.sendSystemMessage(
                Text.translatable("pocketmobs.battle.win", split[2]).formatted(Formatting.BOLD));
            case "move" -> {
                Text pokemon = name(split[2]);
                String move = split[3];
                Text target = name(split[4]);
                player.sendSystemMessage(Text.translatable("pocketmobs.battle.move", pokemon, move, target));
            }
            case "faint" -> {
                Text pokemon = name(split[2]);
                player.sendSystemMessage(Text.translatable("pocketmobs.battle.faint", pokemon));
            }
            case "-damage" -> {
                Text pokemon = name(split[2]);
                player.sendSystemMessage(
                    Text.translatable("pocketmobs.battle.damage", pokemon, split[3]).formatted(Formatting.GRAY));
                if (split[2].startsWith(side.toString())) (side == BattleSide.P1 ? p1Mob : p2Mob).hurt();
            }
            case "error" -> player.sendSystemMessage(Text.literal(split[2]).formatted(Formatting.DARK_RED));
            case "request" -> {
                if (side == BattleSide.P1) p1Request = split[2];
                else p2Request = split[2];
            }
            // TODO: more updates
        }
    }

    public void showRequest(BattleSide side) {
        ServerPlayerEntity player;
        String requestJson;
        if (side == BattleSide.P1) {
            player = player1;
            requestJson = p1Request;
            p1Request = null;
        } else {
            player = player2;
            requestJson = p2Request;
            p2Request = null;
        }

        if (requestJson == null) return;

        Request request = gson.fromJson(requestJson, Request.class);

        if (request.active().length == 0) return;

        MutableText moves = Text.empty();
        for (int i = 0; i < request.active()[0].moves().length; i++) {
            Move move = request.active()[0].moves()[i];
            MutableText moveText = MutableText.create(StringComponent.of(move.move()));
            String command = ">" + side.toString() + " move " + move.id();
            ClickEvent clickEvent = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command);
            Style style = Style.EMPTY.withFormatting(Formatting.BLUE, Formatting.UNDERLINE).withClickEvent(clickEvent);
            moveText.setStyle(style);
            moves.append(moveText);
            if (i != request.active()[0].moves().length - 1) moves.append(", ");
        }
        player.sendSystemMessage(Text.translatable("pocketmobs.battle.request", moves));
    }

    private Text name(String id) {
        Text pokemon = id.startsWith("p1") ? p1Mob.getEntity().getName() : p2Mob.getEntity().getName();
        Text player = id.startsWith("p1") ? player1.getName() : player2.getName();
        return Text.translatable("pocketmobs.battle.pokemon", player, pokemon);
    }

    private record State(Future<?> battleFuture) implements Runnable {
        @Override
        public void run() {
            battleFuture.cancel(true);
        }
    }

    private record BattleFuture(WeakReference<Battle> battle, BlockingQueue<String> writeQueue) implements Runnable {
        private boolean setStream(Showdown.BattleStream stream) {
            Battle battle = this.battle.get();
            if (battle != null) {
                battle.stream = stream;
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void run() {
            Showdown.BattleStream stream = showdown.get().createBattle();
            if (!setStream(stream)) return;

            while (true) {
                try {
                    String message = writeQueue.take();
                    stream.write(message);
                } catch (InterruptedException ex) {
                    return;
                }
            }
        }
    }

    private record Player(String name, String team) {
    }

    private record Request(Active[] active) {
    }

    private record Active(Move[] moves) {
    }

    private record Move(String move, String id, int pp, int maxpp) {
    }
}
