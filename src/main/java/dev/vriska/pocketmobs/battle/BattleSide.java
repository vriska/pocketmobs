package dev.vriska.pocketmobs.battle;

public enum BattleSide {
    P1, P2,
    ;

    @Override
    public String toString() {
        return switch (this) {
            case P1 -> "p1";
            case P2 -> "p2";
        };
    }
}
